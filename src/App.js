import React from 'react';
import "./styles.css"
import Header from './components/Header';
import Carousel from './components/Carousel'
import Categories from './components/Categories'
import Footer from './components/Footer'

const App = () => (
  <div className="App">
    <Header />
    <Carousel/>
    <Categories/>
    <Footer/>
    
  </div>
);


export default App;
