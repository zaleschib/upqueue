import React from 'react';
import './styles.css'
const Categories = () => (
  <div>
      <h3 className="text">Categorias</h3>
  <div className="container">
  <div className="row">
    <div class="col"><img src="./images/blue.jpg" className="img-fluid"/></div>
    <div class="col"><img src="./images/cafedamanha.jpg" className="img-fluid"/></div> 
  </div>
  <div className="row ">
    <div class="col">
        <img src="./images/sobremesas.jpg" className="img-fluid"/>
    </div>
    <div class="col"><img src="./images/comida-americana.png" className="img-fluid"/></div> 
   
  </div>
  <div className="row">
    <div class="col"><img src="./images/comida-brasileira.jpg" className="img-fluid" /></div>
    <div class="col"><img src="./images/comida-francesa.jpg" className="img-fluid"/></div> 
  </div>
  <div className="row">
    <div class="col"><img src="./images/comida-italiana.jpg" className="img-fluid" /></div>
    <div class="col"><img src="./images/comida-japonesa.jpg" className="img-fluid"/></div> 
  </div>
</div>
</div>
);
export default Categories;